package sample;

import com.sun.javafx.menu.MenuItemBase;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;

public class Controller implements Initializable {
    @FXML
    private ColorPicker colorpicker;
    @FXML
    private TextField bsize;
    @FXML
    private Canvas canvas;
    boolean toolSelected= false;
    GraphicsContext brushTool;
    private ButtonBase save;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
         brushTool= canvas.getGraphicsContext2D();
         canvas.setOnMouseDragged( e->{
           double size=Double.parseDouble(bsize.getText());
           double x=e.getX()-size/2;
           double y=e.getY()-size/2;
           if (toolSelected && !bsize.getText().isEmpty()){
           brushTool.setFill(colorpicker.getValue());
           brushTool.fillRoundRect(x,y,size,size,size,size);
           }

         });
     }
     @FXML


    public void toolselected(javafx.event.ActionEvent actionEvent) {
        toolSelected=true;
    }

    public void saving(ActionEvent actionEvent) {
        File file = new File("CanvasImage.png");
        WritableImage image = canvas.snapshot(null, null);
        BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);

        try{
            ImageIO.write(bImage, "png", file);
        }catch (Exception s) {
        }


    }


    }

